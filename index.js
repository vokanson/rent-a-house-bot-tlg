import { Telegraf,Scenes,session,Markup } from 'telegraf'
import dotenv from 'dotenv'
dotenv.config()
import mongoose from "mongoose"
import Lot from './models/schema.js';
import startScene from "./Scences/startScene.js";

// import btnMenuScene from "./Scences/btnMenuScene.js";
const MAIN_MENU = Markup.keyboard([
    ['Главное меню']
]).resize()


// import departure from "./Scences/photoScene.js"

// const orderDates = [0,1,2]
// module.exports = orderDates
let lotImeg

async function main() {
    await mongoose.connect('mongodb://127.0.0.1:27017/houses');
    console.log("connected")
    let lots = await Lot.findOne({title:"lot 1"})
    lotImeg=lots.imeg
    return lotImeg
}

main()




import Calendar from 'telegram-inline-calendar';
import {photoScene} from "./Scences/photoScene.js";
import {conditionScene} from "./Scences/conditionsScene.js";










// bot.start((ctx) => {calendar.startNavCalendar(ctx.message); console.log(ctx.msg.chat.id)});

let calendar
const runBot = async() => {

   const bot = new Telegraf(process.env.BOT_TOKEN)
     calendar = new Calendar(bot, {
        date_format: 'DD-MM-YYYY HH:mm',                     //Datetime result format
        language: 'ru',                                //Language (en/es/de/es/fr/it)
        bot_api: 'telegraf',              //Telegram bot library
        close_calendar: true,                          //Close calendar after date selection
        start_week_day: 1,                             //First day of the week(Sunday - `0`, Monday - `1`, Tuesday - `2` and so on)
        time_selector_mod: true,                      //Enable time selection after a date is selected.
        time_range: "00:00-23:59",                     //Allowed time range in "HH:mm-HH:mm" format
        time_step: "1h",                              //Time step in the format "<Time step><m | h>"
        start_date: new Date(),                             //Minimum date of the calendar in the format "YYYY-MM-DD"
        stop_date: false,                              //Maximum date of the calendar in the format "YYYY-MM-DD"
        custom_start_msg: false                        //Text of the message sent with the calendar/time selector
    });

    // bot.start((ctx) => ctx.reply ("Hi"));
    // console.log(process.env.BOT_TOKEN)
    const stage = new Scenes.Stage([
        startScene,
        photoScene,
        conditionScene,
    ])


        // .extra();



    bot.use(session())
    bot.use(stage.middleware())
    bot.command('start', async (ctx) => {
       await ctx.reply ('Здравствуйте',MAIN_MENU)
        // console.log(1111,ctx.scene)
        ctx.scene.enter('startWizard')
    })

    bot.hears('Главное меню', (ctx) => {
        console.log("action main_menu")
        ctx.scene.enter('startWizard')
        console.log("main_menu")
    })





// bot.use(require('./Composers/start'))
// bot.action('select a date',(ctx)=> {
//     calendar.startNavCalendar(ctx)
// })
// bot.action('view photos',(ctx) => {ctx.reply('Вот фотки')
//     ctx.replyWithPhoto({source: lotImeg})
// })






//     const calArr = calendar.createNavigationKeyboard(new Date())
//     console.log(calArr.inline_keyboard)
//     calArr.inline_keyboard.forEach((element,something) => {
//         element.forEach((s) =>{
//             const f = "n_2023-04-29_0"
//             if (f == s.callback_data){
//                 s.callback_data = ' '
//                 s.text = ' '
//                 console.log(1111111111111111,s.callback_data)
//             }
//         })
//         // console.log("element",element[2].callback_data)
//     })
//     console.log("calArr",calArr.inline_keyboard)

    // const timeAn = calendar.createTimeSelector(new Date())
    // console.log(22222222222222,timeAn.inline_keyboard)
    // console.log(new Date())






//
// // bot.start((ctx) => calendar.startNavCalendar(ctx.message));
//
// bot.on("callback_query", (ctx) => {
//
//     if (ctx.callbackQuery.message.message_id == calendar.chats.get(ctx.callbackQuery.message.chat.id)) {
//         res = calendar.clickButtonCalendar(ctx.callbackQuery);
//         if (res !== -1) {
//             orderDates.push(res)
//             console.log(orderDates)
//             bot.telegram.sendMessage(ctx.callbackQuery.message.chat.id, "You selected: " + res);
//             // console.log("result",result)
//         }
//     }
// });
//
//
// bot.command('orders', (ctx) => {
//     ctx.reply('Вот фотки')
//     ctx.replyWithPhoto({source: lotImeg})
// })
// // bot.start((ctx) => ctx.reply(`Hello ${ctx.from.first_name}, choose the appropriate option ${lot}` ))
// bot.help((ctx) => ctx.reply(text.commands))
//     console.log("dsafgvsrb")
    bot.launch()
    // console.log(7777,bot)

    process.once('SIGINT', () => bot.stop('SIGINT'));
    process.once('SIGTERM', () => bot.stop('SIGTERM'));
}

runBot()

export  {calendar,lotImeg}
