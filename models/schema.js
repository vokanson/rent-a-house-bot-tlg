import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const lotSchema = new Schema({
    title: String,
    price: Number,
    imeg: Array
});

export default mongoose.model('Lot', lotSchema);
