import { Scenes } from "telegraf"


import {viewPhotos} from "../Composers/photos.js"

const photoScene =  new Scenes.WizardScene(
    'showFoto',
    viewPhotos,
)
export {photoScene}