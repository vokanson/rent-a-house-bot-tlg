import { Scenes } from "telegraf"


import {condition} from "../Composers/condition.js"

const conditionScene =  new Scenes.WizardScene(
    'viewConditions',
    condition,
)
export {conditionScene}